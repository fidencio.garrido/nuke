use structopt::StructOpt;
use std::collections::HashSet;
use std::ffi::{OsStr};
use std::fs;
use std::io;
use std::path::PathBuf;

#[derive(StructOpt)]
struct Opt {
	#[structopt(short, long)]
	from: PathBuf,
}

struct Info {
	is_nukable: bool,
	is_dir: bool,
}

struct Nuke<'a> {
	banned_files: HashSet<&'a OsStr>,
	banned_directories: HashSet<&'a OsStr>,
}

impl Nuke<'_> {
	fn is_banned_dir(&self, dir_name: &OsStr, is_directory: bool) -> bool {
		match is_directory {
			true => {
				return self.banned_directories.contains(dir_name);
			}
			false => {
				return self.banned_files.contains(dir_name);
			}
		}
	}
}

fn new_nuke() -> Nuke<'static> {
	let mut banned_files = HashSet::new();
	banned_files.insert(OsStr::new(".DS_Store"));
	banned_files.insert(OsStr::new("Thumbs.db"));
	let mut banned_directories = HashSet::new();
	banned_directories.insert(OsStr::new("node_modules"));
	banned_directories.insert(OsStr::new("WEB-INF"));
	banned_directories.insert(OsStr::new(".git"));
	banned_directories.insert(OsStr::new("bower_components"));
	return Nuke {
		banned_files,
		banned_directories,
	};
}

/// Retrieves metadata about a dir entry, and determines if is a directory and/or if the entry
/// should be removed
fn get_info(entry: &fs::DirEntry, nuke: &Nuke) -> io::Result<Info> {
	let mut is_dir = false;
	if let Ok(file_type) = entry.file_type() {
		is_dir = file_type.is_dir();
	}

	let is_nukable = nuke.is_banned_dir(entry.file_name().as_os_str(), is_dir);
	Ok(Info {
		is_nukable,
		is_dir,
	})
}

/// Reads the content of a folder, and triggers the removal of all blacklisted files and folders
fn traverse(path: &OsStr, nuke: &Nuke) {
	let paths = fs::read_dir(path).unwrap();
	for entry in paths {
		if let Ok(p) = entry {
			let nfo = get_info(&p, &nuke).unwrap();
			if nfo.is_nukable {
				match nuke_path(p.path().as_os_str(), nfo.is_dir) {
					Ok(_) => {
						println!("- {} removed", p.path().display())
					}
					Err(error) => {
						println!("{} cannot be removed: {}", p.path().display(), error);
					}
				}
				continue;
			}
			if nfo.is_dir {
				traverse(p.path().as_os_str(), nuke);
			}
		}
	}
}

/// Handles the removal of a file or directory
fn nuke_path(full_path: &OsStr, is_directory: bool) -> Result<(), io::Error> {
	if is_directory {
		fs::remove_dir_all(full_path)?;
		return Ok(());
	}
	fs::remove_file(full_path)?;
	Ok(())
}

fn main() {
	let nuke = new_nuke();
	let opt = Opt::from_args();
	println!("🚀 Nuking from root: {}", opt.from.display());
	traverse(opt.from.as_os_str(), &nuke);
}